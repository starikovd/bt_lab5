`timescale 1ns/1ps

module rx2 (Reset, rxClk, rxIn, out, error, started);
input logic Reset;
input logic	rxIn;
input logic rxClk;
output logic error;
output logic [7:0]    out;

logic [7:0]    sreg;
logic [3:0]    index;  
output logic 			started;
logic [3:0]    delay;
logic [3:0]		startdelay;


//rxTimer will be for 1/16 of a bit cycle and the values will be checked 
//at the middle of every bit cycle. This will help with the asynchronous communication I think.
always_ff @(posedge rxClk) begin
	//Reset everything
	if(Reset) begin
		sreg <= 0;
		out <= 0;
		index <= 0;
		started <= 0;
		startdelay <= 0;
		delay <= 0;
		error <= 0;
		
		end
	//Check for the start bit.
	//If the input is 0 and the receive process hasn't started then
	//wait for half of the bit cycle
	if(~rxIn & ~started & (startdelay != 7)) begin
		startdelay <= startdelay + 4'b0001;
		end
	//if we have waited for half of the bit cycle and it's still 0 then
	//start the receive process by making "started" true.
	else if (~rxIn & ~started & (startdelay == 7)) begin
		startdelay <= startdelay + 4'b0001;
		started <= 1;
		end
	else if ((startdelay != 0) & (startdelay != 15)) begin
		startdelay <= startdelay + 4'b0001;
		end
	else if (startdelay == 15) begin
		startdelay <= 0;
		end
		
		
	if(started & (startdelay == 0)) begin
		delay <= delay + 4'b0001;
		
		//wait for midpoint of bit receive
		if((delay == 7) & (index != 8)) begin
			//assign the Rx input to current index of the shift register
			sreg[index] <= rxIn;
			end
		
		if((index == 8) & (delay == 7)) begin
			if(~rxIn)
				error <= 1;
			else
				error <= 0;
			end
		
		if(delay == 15) begin 
			//If its index 8 and the delay period is over then:
			if (index == 8) begin
				out <= sreg;
				sreg <= 0;
				index <= 0;
				started <= 0;
				startdelay <= 0;
			end
			else begin
				//increment the index
				index <= index + 4'b0001;
			end

			delay <= 0;
		end
		end
	end
endmodule

module rx2_testbench();
logic Reset;
logic	rxIn;
logic rxClk;
logic error;
logic [7:0] out;
logic started;
rx2 rx_tb(.Reset, .rxClk, .rxIn, .out, .error, .started);
	parameter PERIOD = 6500; // time for the 1/16th of a bit                      
									// Make the clock LONG to test
	initial begin   
		rxClk <= 0;   
		forever #(PERIOD/2) rxClk = ~rxClk; 
	end 
		
	integer i;
	integer k;

	parameter BITDELAY = 104000; //104 microseconds for a 9600 Baud signal
	
	initial begin
		Reset <= 1;
		rxIn <= 1;
		#BITDELAY;
		Reset <= 0;
		rxIn <= 1;
		#BITDELAY;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#1040000;
		
		
		rxIn <= 1;
		#BITDELAY;
		rxIn <= 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		#BITDELAY;
		#1040000;

		Reset <= 1;
		#BITDELAY;
		Reset <= 0;
		#BITDELAY;
		
		
		rxIn = 1;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		rxIn = 0;
		#BITDELAY;
		rxIn = 1;
		#BITDELAY;
		rxIn = 1;
		#1040000;
		
		$stop();
	end
endmodule 


