	module clock_divider (clock, delay, outputclock);
	input logic				clock;
	input logic [12:0] delay;
	logic [12:0] divided_clocks;
	output logic outputclock;
	initial
		divided_clocks <= 0;
		
	always_ff @(posedge clock) begin
		divided_clocks <= divided_clocks + 12'b000000000001;
		if(divided_clocks == delay/2) begin
			outputclock <= 1;
			end
		else if (divided_clocks >= delay) begin
			outputclock <= 0;
			divided_clocks <= 0;
		end
	end

endmodule