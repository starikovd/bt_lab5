//Top-level module that defines the I/O's for the DE-1 board
`timescale 1ns/1ps
module DE1_SoC (CLOCK_50, LEDR, SW, GPIO_1, HEX0);
	input logic CLOCK_50;
	input logic [9:0] SW;
	output logic [9:0] LEDR;
	input logic [35:0] GPIO_1;
	output logic [6:0] HEX0;

	//use a 1540000Hz clock based off the 50Mhz to sample each bit 16 times.
	logic clk_154kHz;
	clock_divider rx_clock (CLOCK_50, 326, clk_154kHz);

	rx2 receive(.Reset(SW[9]), .rxClk(clk_154kHz), .rxIn(GPIO_1[0]), .out(LEDR[7:0]), .error(LEDR[9]), .started(LEDR[8]));

	hex_number hex0_driver(.data(LEDR[7:0]), .HEX(HEX0));
	



endmodule

module DE1_SoC_testbench();
rx2_testbench rx_tb();
logic Clock;
logic [9:0] SW, LEDR;
logic [6:0] HEX0;
logic [35:0] GPIO_1;
DE1_SoC dut (.CLOCK_50(Clock), .LEDR, .SW, .GPIO_1, .HEX0);  
	parameter PERIOD = 20; // 20 ns for clock of 50MHz                      
									// Make the clock LONG to test
	initial begin   
		Clock <= 0;   
		forever #(PERIOD/2) Clock = ~Clock; 
	end 
		
	integer i;
	integer k;

	parameter BITDELAY = 104000; //104 microseconds for a 9600 Baud signal
	
	initial begin
		SW[8:0] = 9'b001000001;
		SW[9] <= 1;
		GPIO_1[0] <= 1;
		#BITDELAY;
		SW[9] <= 0;
		GPIO_1[0] <= 1;
		#BITDELAY;
		#BITDELAY;
		GPIO_1[0] = 0;
		#BITDELAY;
		GPIO_1[0] = 1;
		#BITDELAY;
		GPIO_1[0] = 0;
		#BITDELAY;
		GPIO_1[0] = 0;
		#BITDELAY;
		GPIO_1[0] = 0;
		#BITDELAY;
		GPIO_1[0] = 0;
		#BITDELAY;
		GPIO_1[0] = 0;
		#BITDELAY;
		GPIO_1[0] = 0;
		#BITDELAY;
		GPIO_1[0] = 0;
		#BITDELAY;
		GPIO_1[0] = 1;
		#1040000;
		
	end
endmodule 


	



